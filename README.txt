Instructions to Execute:

	(1) The program you need to run is main.py
	(2) Usage:
		python main.py <optional_filename_argument>

	If no filename argument is provided, the program uses the SampleInput.txt file present in the same location as input. If the user wishes to provide own input, create a file and add one natural language expression on each line of the file. Save the file and pass the filename as the command line argument.
	
	The program supports the following operations - addition, subtraction, multiplication, division. The program also supports parenthesization (using '(' and ')' or 'bopen' and 'bclose'). You can write expressions like:
		four by bopen 2 + three bclose
		8 divided by 2
		fourpoint2plusninepointzero
		2 * (three - bopen three million seventeen thousand+5))
		
	To understand all capabilities, please refer to the SampleInput.txt file and peruse the various test cases. I believe the program is quite robust and solves all cases provided in the handout and even more. Also, please do take a look at the other text files in the source directory. They will reveal all the possibilities.
Note: Please do not use "and"

General Outline:

	(1) [Cleaner.py] Clean the expression - This involves inserting spaces wherever required and removing extra spaces. Also, this converts natural language number names to then numeric equivalents only partially. For instance, if "two hundred thousand nineteen" is passed, it is converted to [2, 100, 1000, 19].

	(2) [Numerize.py] Make the expression a valid infix expression. For definition of infix expression, please consult - http://en.wikipedia.org/wiki/Infix_notation. This involves an algorithm to convert a full string to a number. For instance, "two hundred seventeen thousand and forty three point 9 seven two" is converted to "217043.972"

	(3) [Infix.py] This now takes a well-formed infix expression and evaluate it. This is done by converting it to postfix notation (using the Shunting Yard Algorithm - http://en.wikipedia.org/wiki/Shunting-yard_algorithm) and using a stack to evaluate the postfix expression. Postfix notation is also known as Reverse Polish Notation (http://en.wikipedia.org/wiki/Reverse_Polish_notation).
