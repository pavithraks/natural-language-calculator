from Utils import *
from Constants import *
import re

class Cleaner:
    '''
        Contains methods to convert the input into a standard form with
        the required amount spaces at the right places

        Usage:
            x = Cleaner("(two + 24 * 3    minus sixty)")
            print x.get_cleaned_string_as_tokens()
        Expected:
            ['(', 'two', '+', '24', '*', '3', '-', 'sixty', ')']
    '''
    
    def __init__(self, expression):
        self.__expression = expression.lower()

    def __multiple_replace(self,string, rep_dict):
        '''
            # From http://stackoverflow.com/a/15448887
            Replaces values for multiple substrings by taking a dictionary that has
            substring as keys and strings to be replaced as values. 
        '''
        pattern = re.compile("|".join([re.escape(k) for k in rep_dict.keys()]), re.M)
        return pattern.sub(lambda x: rep_dict[x.group(0)], string)

    def __replace_operator_names(self):
        '''
            Replace "plus" with "+", "minus" with "-" and so on
        '''
        operators_map = Utils.get_map_from_csv_file(Constants.OPERATORS_NAME_FILE)

        for op_name in operators_map:
            self.__expression = self.__expression.replace(op_name, operators_map[op_name])

        self.__operators = operators_map.values()

    def __add_spaces_around_operators(self):
        '''
            Tranform expressions like "(2+ 3*5)" to "( 2 + 3 * 5 )"
        '''
        padded_expression = ""
        for char in self.__expression:
            if char in self.__operators:
                padded_expression = padded_expression + " "  + char + " "
            else:
                padded_expression = padded_expression + char

        self.__expression = padded_expression

    def __get_replace_map(self):
        '''
            Loads the replace map file into a dictionary
        '''
        replace_map = Utils.get_map_from_csv_file(Constants.REPLACE_MAP_FILE)
        replace_map["point"] = " point "
        return replace_map

    def __add_spaces_around_tokens(self):
        tokens_map = Utils.get_map_from_csv_file(Constants.NUMBER_STRINGS_FILE)
        all_tokens = tokens_map.keys()
        all_tokens.sort(key=lambda item: (-len(item), item))
        for token in all_tokens:
            self.__expression=self.__expression.replace(token," "+token+" ")
        self.__expression = self.__multiple_replace(self.__expression, self.__get_replace_map())
    
    def __remove_multiple_spaces(self):
        '''
            # From http://stackoverflow.com/a/2077944
            Converts "     2 + 3 * 5   " into "2 + 3 * 5"
        '''
        self.__expression = " ".join(self.__expression.split())

    def __replace_number_names(self):
        '''
            Replaces number strings with their numeric equivalents
        '''
        numbers_map = Utils.get_map_from_csv_file(Constants.NUMBER_STRINGS_FILE)

        number_names = numbers_map.keys()
        # Below from http://stackoverflow.com/a/4659539
        number_names.sort(key=lambda item: (-len(item), item))

        for number_name in number_names:
            self.__expression = self.__expression.replace(number_name, numbers_map[number_name])

    def get_cleaned_string_as_tokens(self):
        self.__replace_operator_names()
        
        self.__add_spaces_around_operators()

        self.__add_spaces_around_tokens()

        self.__remove_multiple_spaces()

        self.__replace_number_names()

        return self.__expression.split(' ')
