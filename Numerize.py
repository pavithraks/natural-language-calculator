from Utils import *
from Constants import *
import math

class Numerize:

    '''
        Reads a cleaned string expression and replaces text with numbers
        using a dictionary.

        Usage:
            x = Numerize(['twelve','thousand','*','sixteen'])
            print x.get_numerized_list()
        Expected: ['12000','*','16']
    '''

    def __is_power(self, num, base):
        '''
            From http://stackoverflow.com/a/15352628
            Checks if 'num' is an integer power of 'base'
        '''
        power = int (math.log (num, base) + 0.5)
        return base ** power == num and num != 1.0

    def __init__(self, cleaned_tokens):
        self.__cleaned_tokens = cleaned_tokens
        self.__all_operators = Utils.get_map_from_csv_file(Constants.OPERATORS_NAME_FILE).values()

    def __compute_numerical_value(self, numerical_tokens):
        '''
            This is the main algorithm that forms the numerical value of a natural language
            string. For instance, "seventeen thousand point nine 2 six 2" will be converted
            to 17000.9262.
        '''
        val1 = 0.0
        val2 = 0.0
        for token in numerical_tokens:
            if token == 'point' or token == '.':
                val3 = float(numerical_tokens[numerical_tokens.index(token)+1]) * 10
                for num in numerical_tokens[numerical_tokens.index(token) + 2:]:
                    val3 = (val3 + float(num)) * 10
                return (val1+val2+val3/pow(10,len(numerical_tokens[numerical_tokens.index(token):])))
            else:
                token_val = float(token)
                if self.__is_power(token_val, 10):
                    # Hack to handle cases of implicit "one" like "hundred thousand"
                    if val1 == 0.0:
                        val1 = 1.0

                    val1 = val1 * token_val

                    if token_val != 100.0:
                        val2 = val2 + val1
                        val1 = 0.0
                else:
                    val1 = val1 + token_val
        return (val2 + val1)

    def generate_expression(self):
        '''
            The public function that generates a well-formed infix expression with all numerical
            values. This is done by repeated calling of the compute_numerical_value function.

            The code below goes over all tokens in the input expression and passes whole strings
            to the compute_numerical_value function, which gives the numerical representation
        '''
        current_val = []
        infix_tokens = []
        for token in self.__cleaned_tokens:
            if token in self.__all_operators:
                if len(current_val) > 0:
                    infix_tokens.append(self.__compute_numerical_value(current_val))
                infix_tokens.append(token)
                current_val = []                
            else:
                current_val.append(token)
        if len(current_val) > 0:
            infix_tokens.append(self.__compute_numerical_value(current_val))

        return infix_tokens
