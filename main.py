from Infix import *
from Cleaner import *
from Numerize import *

def evaluate(exp):
    print "Input Expression:",exp
    cleaner = Cleaner(exp)

    numerize = Numerize(cleaner.get_cleaned_string_as_tokens())

    final_exp = numerize.generate_expression()
    infix = Infix(final_exp)

    print " ".join([str(tok) for tok in final_exp])
    print "Value:",infix.getValue()

    print "-------"


def main():
    '''
        Wrapper that is used to read natural language expressions from an
        input file and evaluate them.
    '''
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = Constants.SAMPLE_TEST_FILE

    test_strings = Utils.get_lines_from_file(filename)

    for s in test_strings:
        try:
            evaluate(s)
        except Exception as e:
            print "Invalid Input"
            print "-------"
        
if __name__ == '__main__':
    main()
    




