from Utils import *
from Constants import *

class Infix:

    '''
        Reads a tokenized infix expression and evaluates it.

        Usage:
            x = Infix(['2', '+', '3', '*', '5'])
            print x.get_value()
        Expected:
            17.0
    '''

    def __init__(self, infix_tokens):
        self.__infix_tokens = infix_tokens
        self.__precedence_map = Utils.get_map_from_csv_file(Constants.PRECEDENCE_FILE)
        self.__all_operators = Utils.get_map_from_csv_file(Constants.OPERATORS_NAME_FILE).values()

    def __precedence(self, operator):
        '''
            Returns the precedence of the operator
        '''
        if self.__precedence_map.get(operator):
            return int(self.__precedence_map[operator])
        else:
            return 0

    def __is_operator(self, token):
        '''
            Checks if token is a mathematical operator
        '''
        return token in self.__all_operators

    def __get_postfix(self, infix_tokens):
        '''
            Converts the given infix expression to postfix notation
        '''
        op_stack=[]
        postfix=[]
        op_stack.append("#")
        for token in infix_tokens:
            if token == "(":
                op_stack.append(token)
            elif token == ")":
                while op_stack[len(op_stack)-1]!="(":
                    postfix.append(op_stack.pop())
                op_stack.pop()
            elif self.__is_operator(token):
                while self.__precedence(token) <= self.__precedence(op_stack[len(op_stack)-1]):
                    postfix.append(op_stack.pop())
                op_stack.append(token)
            else:
                postfix.append(token)
        
        while len(op_stack)!=1:
            postfix.append(op_stack.pop())
            
        return postfix

    def __evaluate_postfix(self, postfix_tokens):
        '''
            Evaluates the postfix expression
        '''
        stack = []

        for token in postfix_tokens:
            if token == "+":
                results = stack.pop() + stack.pop()
                stack.append(results)
            elif token == "-":
                results = -(stack.pop() - stack.pop())
                stack.append(results)
            elif token == "*":
                results = stack.pop() * stack.pop()
                stack.append(results)
            elif token == "/":
                results = 1 / (stack.pop() / stack.pop())
                stack.append(results)
            else:
                stack.append(float(token))

        return stack.pop()       
                
    def getValue(self):
        '''
            Public function use to get the value of a well-formed infix expression
        '''
        postfix_tokens = self.__get_postfix(self.__infix_tokens)
        return self.__evaluate_postfix(postfix_tokens)
