class Constants:

    '''
        Contains all constants used in the program
    '''

    OPERATORS_NAME_FILE = "OperatorNames.csv"

    NUMBER_STRINGS_FILE = "NumberStrings.csv"

    SAMPLE_TEST_FILE = "SampleInput.txt"

    REPLACE_MAP_FILE = "ReplaceMap.csv"

    PRECEDENCE_FILE = "Precedence.csv"

    
